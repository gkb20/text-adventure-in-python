import random


class Weapon(object):
    # max_damage = 0

    def __init__(self, max_damage):
        super(object, self).__init__()
        self.max_damage = max_damage

    def damage(self, monster):
        actual_damage = random.randint(self.max_damage / 2, self.max_damage)
        monster.take_damage(actual_damage)


class Knife(Weapon):
    pass


class RustyKnife(Knife):
    def __init__(self):
        super(RustyKnife, self).__init__(10)

    def __str__(self):
        return "A dirty old broken rusty knife"
