import random


class Monster(object):
    is_alive = True

    def __init__(self, health):
        self.health = health

    def take_damage(self, amount):
        self.health -= amount
        print("Monster takes", str(amount), "of damage")
        if self.health <= 0:
            self.health = 0
            print("Monster is dead!")
            self.is_alive = False


class SmallMonster(Monster):
    def __init__(self):
        super(SmallMonster, self).__init__(random.randint(10, 20))
