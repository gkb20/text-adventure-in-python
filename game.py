# from __Main_menu__ import *
from character import *
from monster import *
from weapons import *
# from world import *


class Game:
    # world = World()
    # location = Location()
    hero = Character()
    small_monster = SmallMonster()

    def part_1(self):
        hero = self.hero
        small_monster = self.small_monster
        # action = main_menu()
        print("monster health was", small_monster.health)
        action = "attack"
        found_a_knife = RustyKnife()
        hero.take_weapon(found_a_knife)
        print("hero's weapon is ", hero.weapon)
        while small_monster is not None:
            if action == "attack":
                hero.attack(small_monster)
                print("Hero health is now " + str(hero.health.health))
                print("monster health now is", small_monster.health)
                if not small_monster.is_alive:
                    small_monster = None
                print("yes")

            else:
                print("no")
        print("You vanquished the small monster! Hurray for you!")
game = Game()
game.part_1()
