from health import *


class Character:

    health = Health()
    weapon = None

    def take_weapon(self, weapon):
        self.weapon = weapon

    def attack(self, monster):
        if self.weapon is None:
            print("You attack with no weapons?!")
            self.health.monster_high_damage()
        else:
            self.weapon.damage(monster)
            print("You smash that thing")

        if monster.is_alive:
            print("The monster takes a swipe at you landing a critical hit!")
            self.health.monster_low_damage()
