class World:
    locations = []
    locations.append(Location("A meadow"))
    locations.append(Location("A cave"))
    locations.append(Location("A locked door"))

class Location:
    def __init__(self, description):
        self.description = description