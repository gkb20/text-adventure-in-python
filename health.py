import random


class Health:
    health = 100

    def monster_low_damage(self):
        low_counter = random.randint(0, 15)
        self.health = self.health - low_counter
        print("you take", str(low_counter), "of damage")
        return low_counter

    def monster_high_damage(self):
        high_counter = random.randint(20, 30)
        self.health = self.health - high_counter
        print("you take", str(high_counter), "of damage")
        return high_counter
